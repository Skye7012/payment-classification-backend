using System.IO;
using System.Threading.Tasks;

namespace CommonBlocks.Cryptography.Interfaces
{
    public interface ICryptoHasher
    {
        public string HashAlgorithm { get; }

        public byte[] GenerateHash(byte[] data);
        
        public Task<byte[]> GenerateHashAsync(byte[] data);

        public Task<byte[]> GenerateHashAsync(Stream data);
        
    }
}