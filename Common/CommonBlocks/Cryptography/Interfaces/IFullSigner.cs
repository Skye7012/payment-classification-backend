namespace CommonBlocks.Cryptography.Interfaces
{
    public interface IFullSigner : ISignatureGenerator, ISignatureValidator
    {
        
    }
}