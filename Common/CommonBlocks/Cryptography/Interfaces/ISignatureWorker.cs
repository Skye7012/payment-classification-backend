namespace CommonBlocks.Cryptography.Interfaces
{
    /// <summary>
    /// Describes general cryptographic sign worker
    /// </summary>
    public interface ISignatureWorker
    {
        /// <summary>
        /// Current algorithm
        /// </summary>
        public string SignerAlgorithm { get; }
    }
}