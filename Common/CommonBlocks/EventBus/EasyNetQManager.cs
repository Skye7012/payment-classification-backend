using System;
using System.Threading.Tasks;
using EasyNetQ;
using EasyNetQ.Internals;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CommonBlocks.EventBus
{
    public class EasyNetQManager
    {
        private readonly IBus _rabbitMqBus;
        private readonly ILogger<EasyNetQManager> _logger;
        private readonly IEventBusSubscriptionsManager _subsManager;
        private readonly int _retryCount;
        private readonly IServiceProvider _serviceProvider;

        private string _queueName;

        public EasyNetQManager(IBus rabbitMqBus, ILogger<EasyNetQManager> logger,
            IEventBusSubscriptionsManager subsManager, IServiceProvider serviceProvider,
            string queueName = "SPECIFY_NAME", int retryCount = 5)
        {
            _rabbitMqBus = rabbitMqBus ?? throw new ArgumentNullException(nameof(rabbitMqBus));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _serviceProvider = serviceProvider;
            _subsManager = subsManager ?? new InMemoryEventBusSubscriptionsManager();
            _queueName = queueName;
            _retryCount = retryCount;
            _subsManager.OnEventRemoved += SubsManager_OnEventRemoved;
        }

        private void SubsManager_OnEventRemoved(object? sender,
            AwaitableDisposable<ISubscriptionResult> awaitableDisposable)
        {
        }

        public void Subscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>
        {
            var eventName = _subsManager.GetEventKey<T>();
            var subscriptionResult = _rabbitMqBus.PubSub.SubscribeAsync<T>(_queueName, handleMessage);
            _logger.LogInformation("Subscribing to event {EventName} with {EventHandler}", eventName,
                typeof(TH).GetGenericTypeName());

            _subsManager.AddSubscription<T, TH>(subscriptionResult);
        }

        private async Task handleMessage<T>(T msg) where T : IntegrationEvent
        {
            if (!_subsManager.HasSubscriptionsForEvent<T>())
            {
                _logger.LogWarning("No subscription for RabbitMQ event: {EventName}", _subsManager.GetEventKey<T>());
                return;
            }

            using var serviceScope = _serviceProvider.CreateScope();

            foreach (var subscription in _subsManager.GetHandlersForEvent<T>())
            {
                var concreteType = typeof(IIntegrationEventHandler<>).MakeGenericType(typeof(T));
                var handler = serviceScope.ServiceProvider.GetService(concreteType);
                
                if (handler is null)
                {
                    _logger.LogWarning("Couldn't find registered handler {EventHandlerName} for event {EventName}",
                        subscription.HandlerType.Name, _subsManager.GetEventKey<T>());
                    continue;
                }

                await Task.Yield();
                await (Task) concreteType.GetMethod("Handle").Invoke(handler, new object?[] {msg});
            }
        }
    }
}