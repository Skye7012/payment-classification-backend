﻿using System;
using System.Threading.Tasks;
using CommonBlocks.Cryptography.Implementations;
using CommonBlocks.Cryptography.Interfaces;
using ConsoleAppFramework;
using CryptoTools.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CryptoTools
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await Host.CreateDefaultBuilder()
                .ConfigureServices((context, collection) =>
                {
                    collection.AddScoped<ICryptoHasher, CryptoHasher>();
                    collection.AddScoped<IFullSigner, Ed25519Signer>();
                })
                .RunConsoleAppFrameworkAsync(args);
        }
    }
}