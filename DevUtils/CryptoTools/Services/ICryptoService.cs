namespace CryptoTools.Services
{
    public interface ICryptoService
    {
        bool VerifySignature();
    }
}