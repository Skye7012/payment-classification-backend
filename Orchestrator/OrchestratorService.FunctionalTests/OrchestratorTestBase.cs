using System;
using System.IO;
using CommonBlocks;
using k8s;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using OrchestratorService.Infrastructure.Data;

namespace OrchestratorService.FunctionalTests
{
    public class OrchestratorTestBase
    {
        public TestServer CreateTestServer()
        {
            var basePath = Path.GetDirectoryName(typeof(OrchestratorTestStartup).Assembly.Location);
            var hostBuilder = new WebHostBuilder()
                .UseContentRoot(basePath)
                .ConfigureAppConfiguration(cb =>
                {
                    cb
                        .AddJsonFile("appsettings.json", optional: false)
                        .AddEnvironmentVariables();
                })
                .UseStartup<OrchestratorTestStartup>();

            var server = new TestServer(hostBuilder);

            server.Host.MigrateDbContext<OrchestratorDbContext>((context, services) =>
            {
                context.Database.EnsureCreated();
            });

            return server;
        }

        public static class GetRequests
        {
            public static string GetWithPassword(string password)
                => $"api/v1/TestShowcase?password={password}";
        }
    }
}