using System;
using System.Net;
using Xunit;

namespace OrchestratorService.FunctionalTests
{
    public class OrchestratorTestCases : OrchestratorTestBase
    {
        [Fact]
        public async void Get_should_return_ok_and_secret_when_password_is_correct()
        {
            using var server = CreateTestServer();
            var response = await server.CreateClient().GetAsync(GetRequests.GetWithPassword("correct"));
            response.EnsureSuccessStatusCode();
            Assert.Equal("secret", await response.Content.ReadAsStringAsync());
        }

        [Fact]
        public async void Get_should_return_forbidden_and_password_when_password_is_incorrect()
        {
            using var server = CreateTestServer();
            var password = "wrong_password";
            var response = await server.CreateClient().GetAsync(GetRequests.GetWithPassword(password));
            Assert.Equal(HttpStatusCode.Forbidden, response.StatusCode);
            Assert.Equal(password, await response.Content.ReadAsStringAsync());
        }
    }
}