using Microsoft.Extensions.Configuration;

namespace OrchestratorService.FunctionalTests
{
    public class OrchestratorTestStartup : Startup
    {
        public OrchestratorTestStartup(IConfiguration configuration) : base(configuration)
        {
        }
    }
}