using System;
using System.Threading.Tasks;
using CommonBlocks.EventBus;
using OrchestratorService.Application.IntegrationEvents.Events;

namespace OrchestratorService.Application.IntegrationEvents.EventHandlers
{
    public class TestEventHandler : IIntegrationEventHandler<TestEvent>
    {
        public async Task Handle(TestEvent @event)
        {
            Console.WriteLine($"Recieved {@event.Id}, Message {@event.TestString}");
        }
    }
}