using System;

namespace OrchestratorService.Domain.Models
{
    public enum ProcessingStatus
    {
        Pending,
        Executing,
        Succeeded,
        Failed
    }
    
    public class ProcessingStatusSnapshot : BaseEntity
    {
        public ProcessingStatus Status { get; set; }
        
        public string Message { get; set; }
        
        public int WorkObjectId { get; set; }
        
        public string InitiatedBy { get; set; }
        
        public DateTime InitiatedAt { get; set; }
    }
}