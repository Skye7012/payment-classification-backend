using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrchestratorService.Domain.Models;

namespace OrchestratorService.Infrastructure.Data.Configuration
{
    public class ProcessingStatusSnapshotConfiguration : IEntityTypeConfiguration<ProcessingStatusSnapshot>
    {
        public void Configure(EntityTypeBuilder<ProcessingStatusSnapshot> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Ignore(b => b.DomainEvents);
            
            builder.Property(b => b.Id)
                .UseHiLo("workobjectSeq", OrchestratorDbContext.DEFAULT_SCHEMA);
            
            
        }
    }
}