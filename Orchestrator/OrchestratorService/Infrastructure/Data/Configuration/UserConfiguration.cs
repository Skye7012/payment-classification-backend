using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrchestratorService.Domain.Models;

namespace OrchestratorService.Infrastructure.Data.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Ignore(b => b.DomainEvents);
            
            builder.Property(b => b.Id)
                .UseHiLo("userSeq", OrchestratorDbContext.DEFAULT_SCHEMA);
        }
    }
}