using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Npgsql;
using OrchestratorService.Domain.Models;
using OrchestratorService.Infrastructure.Extensions;
using OrchestratorService.Infrastructure.Repositories;

namespace OrchestratorService.Infrastructure.Data
{
    public class OrchestratorDbContext : DbContext, IUnitOfWork
    {
        public const string DEFAULT_SCHEMA = "orchestrartor";
        public DbSet<User> Users;
        public DbSet<WorkObject> WorkObjects;
        public DbSet<ProcessingStatusSnapshot> ProcessingStatusSnapshots;

        private readonly IMediator _mediator;
        private IDbContextTransaction? _currentTransaction;

        public IDbContextTransaction? GetCurrentTransaction() => _currentTransaction;

        public bool HasActiveTransaction => _currentTransaction != null;
        
        public OrchestratorDbContext(DbContextOptions<OrchestratorDbContext> options) : base(options) { }

        public OrchestratorDbContext(DbContextOptions<OrchestratorDbContext> options, IMediator mediator) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            NpgsqlConnection.GlobalTypeMapper.MapEnum<ProcessingStatus>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasPostgresEnum<ProcessingStatus>();
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
        
        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await _mediator.DispatchDomainEventsAsync(this);


            var result = await base.SaveChangesAsync(cancellationToken);

            return true;
        }
    }
}