using System.IO;
using System.Threading.Tasks;

namespace OrchestratorService.Infrastructure.Services
{
    public interface ICryptoService
    {
        public int SymmetricKeyLength { get; }
        public int SignatureKeyLength { get; }
        
        public Task<byte[]> SignData(byte[] data, byte[] privateKey);
        public Task<byte[]> VerifyData(byte[] data, byte[] publicKey);

        public Task<byte[]> EncryptSymmetrically(byte[] data, byte[] key);
        public Task EncryptSymmetrically(Stream inputStream, byte[] key, Stream outputStream);
    }
}