﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace OrchestratorService.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "orchestrartor");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:processing_status", "pending,executing,succeeded,failed");

            migrationBuilder.CreateSequence(
                name: "userSeq",
                schema: "orchestrartor",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "workobjectSeq",
                schema: "orchestrartor",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SequenceHiLo),
                    PublicEcdsaKey = table.Column<byte[]>(type: "bytea", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkObject",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SequenceHiLo),
                    FileName = table.Column<string>(type: "text", nullable: true),
                    CreatedById = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkObject", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProcessingStatusSnapshot",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SequenceHiLo),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Message = table.Column<string>(type: "text", nullable: true),
                    WorkObjectId = table.Column<int>(type: "integer", nullable: false),
                    InitiatedBy = table.Column<string>(type: "text", nullable: true),
                    InitiatedAt = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessingStatusSnapshot", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProcessingStatusSnapshot_WorkObject_WorkObjectId",
                        column: x => x.WorkObjectId,
                        principalTable: "WorkObject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProcessingStatusSnapshot_WorkObjectId",
                table: "ProcessingStatusSnapshot",
                column: "WorkObjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProcessingStatusSnapshot");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "WorkObject");

            migrationBuilder.DropSequence(
                name: "userSeq",
                schema: "orchestrartor");

            migrationBuilder.DropSequence(
                name: "workobjectSeq",
                schema: "orchestrartor");
        }
    }
}
