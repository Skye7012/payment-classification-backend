using System;
using System.Linq;
using System.Reflection;
using CommonBlocks.EventBus;
using EasyNetQ;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using OrchestratorService.Application.Controllers;
using OrchestratorService.Application.IntegrationEvents.EventHandlers;
using OrchestratorService.Application.IntegrationEvents.Events;
using OrchestratorService.Infrastructure.Data;

namespace OrchestratorService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                //Я на эту строку минут 40 потратял, ковыряя почему в функциональном тесте 404
                .AddApplicationPart(typeof(TestShowcaseController).Assembly);
            services.AddHealthChecks();

            services
                .AddCustomHealthChecks(Configuration)
                .AddRabbitMQ(Configuration)
                .AddDbContexts(Configuration);


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "OrchestratorService", Version = "v0.1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "OrchestratorService v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/hc", new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
                endpoints.MapHealthChecks("/liveness", new HealthCheckOptions
                {
                    Predicate = r => r.Name.Contains("self")
                });
            });

            ConfigureRabbitMQ(app, Configuration);
        }

        public void ConfigureRabbitMQ(IApplicationBuilder app, IConfiguration configuration)
        {
            var easyNetQManager = app.ApplicationServices.GetRequiredService<EasyNetQManager>();
            
            easyNetQManager.Subscribe<TestEvent, TestEventHandler>();
        }
    }

    public static class StartupException
    {
        public static IServiceCollection AddDbContexts(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<OrchestratorDbContext>(options =>
            {
                options.UseNpgsql(configuration["DbConnectionString"], builder =>
                {
                    builder.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
                    builder.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30),
                        errorCodesToAdd: null);
                });
            });
            return services;
        }

        public static IServiceCollection AddCustomHealthChecks(this IServiceCollection services,
            IConfiguration configuration)
        {
            var builder = services.AddHealthChecks();

            builder.AddCheck("self", () => HealthCheckResult.Healthy())
                .AddNpgSql(configuration["DbConnectionString"], name: "orchestratorDB-check",
                    tags: new[] {"orchestratorDB"})
                .AddRabbitMQ($"amqp://{configuration[$"EventBusConnection"]}", name: "orchestrator-rabbitmq-check",
                    tags: new[] {"rabbitmq"});

            return services;
        }

        public static IServiceCollection AddRabbitMQ(this IServiceCollection services, IConfiguration configuration)
        {
            var host = configuration["EventBusConnection"] ??
                       throw new ArgumentException("EventBusConnection is empty");
            var username = configuration["EventBusUserName"] ??
                           throw new ArgumentException("EventBusUserName is empty");
            var password = configuration["EventBusPassword"] ??
                           throw new ArgumentException("EventBusPassword is empty");

            services.RegisterEasyNetQ($"host={host};username={username};password={password}");
            
            services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();
            
            services.AddSingleton<EasyNetQManager>(sp =>
            {
                var queueName = configuration["QueueName"] ?? "Orchestrator";
                var rabbitMqBus = sp.GetRequiredService<IBus>();
                var logger = sp.GetRequiredService<ILogger<EasyNetQManager>>();
                var subsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();

                return new EasyNetQManager(rabbitMqBus, logger, subsManager, sp, queueName);
            });


            services.AddTransient<IIntegrationEventHandler<TestEvent>, TestEventHandler>();

            return services;
        }
    }
}